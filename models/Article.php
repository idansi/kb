<?php

namespace app\models;
use yii\db\ActiveRecord;//HW5
use yii\db\Expression;//HW5
use yii\behaviors\BlameableBehavior;//HW5
use dosamigos\taggable\Taggable;
use Yii;

/**
 * This is the model class for table "article".
 * @property int $id
 * @property string $title
 * @property string $descriptin
 * @property string $body
 * @property int $author_id
 * @property int $editor_id
 * @property int $category_id
 * @property string $created_at
 * @property string $updated_at
 * @property int $created_by
 * @property int $updated_by
 */
class Article extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['body'], 'string'],
            [['author_id', 'editor_id', 'category_id', ], 'integer'],
            [['tagNames'], 'safe'],
           // [['created_at', 'updated_at'], 'safe'],
            [['title', 'descriptin'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'descriptin' => 'Descriptin',
            'body' => 'Body',
            'author_id' => 'Author ID',
            'editor_id' => 'Editor ID',
            'category_id' => 'Category',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    public function getCategory(){
        return $this->hasOne(Category::className(),['id'=>'category_id'] );


    }

    /**
 * @return \yii\db\ActiveQuery
 */
public function getTags()
{
    return $this->hasMany(Tag::className(), ['id' => 'tag_id'])->viaTable('article_tag_assn', ['article_id' => 'id']);
}

   public function behaviors()//HW5
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => new Expression('NOW()'),
            ],
            'blameable' => [
                'class' => BlameableBehavior::className(),
                'createdByAttribute' => 'created_by',
                'updatedByAttribute' => 'updated_by',
                ],
             
            Taggable::className(),
        ];


     }
}
