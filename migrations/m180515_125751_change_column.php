<?php

use yii\db\Migration;

/**
 * Class m180515_125751_change_column
 */
class m180515_125751_change_column extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->alterColumn('article','created_at',$this->integer(11));
        $this->alterColumn('article','updated_at',$this->integer(11));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m180429_162734_change_column cannot be reverted.\n";
        $this->alterColumn('article','created_at',$this->timestamp());
        $this->alterColumn('article','updated_at',$this->timestamp());
        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m180515_125751_change_column cannot be reverted.\n";

        return false;
    }
    */
}
